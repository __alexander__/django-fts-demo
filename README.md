# django-fts-demo

Demo project for the blog post: https://alexanderae.com/django-postgres-avanzado-1.html

# Requirements

- Python 3.6+
- PostgreSQL 9.5+

# Books dataset

books.csv from: https://gitlab.com/librera/librera-books-10k

# Contributors

Alexander Ayasca Esquives
