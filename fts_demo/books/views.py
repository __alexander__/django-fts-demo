from django.shortcuts import render

from .models import Book


def search(request):
    q = request.GET.get('q', '')
    if q:
        books = Book.objects.filter(title__icontains=q)
    return render(request, 'search.html', locals())
