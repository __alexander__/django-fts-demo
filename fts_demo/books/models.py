from django.db import models


class Book(models.Model):
    isbn = models.CharField('ISBN 13', max_length=13)
    title = models.CharField(max_length=255)
    authors = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.title
